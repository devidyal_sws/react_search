import { searchService } from '../_services/';
//import { history } from '../_helpers';

export const searchAction = {
    getAuthors
};

function getAuthors(code){
    return dispatch => {
        let apiEndpoint = 'authors/'+code+'.json';
        searchService.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            dispatch(changeAuthorsList(response.data.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}


export function changeAuthorsList(author){
    return{
        type: "FETECHED_ALL_AUTHORS",
        author: author
    }
}
