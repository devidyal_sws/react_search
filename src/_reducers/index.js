import { combineReducers } from 'redux';
import { authors } from './search.reducers';


const rootReducer = combineReducers({
  authors
});

export default rootReducer;
