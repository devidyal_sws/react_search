import React, { Component } from "react";
import { MDBCol, MDBContainer, MDBRow,MDBNavbar,MDBNavbarBrand } from "mdbreact";
import { connect } from 'react-redux';
import { searchAction } from '../_actions';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {searchCode: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({searchCode: event.target.value});
  }

  getAuthors = event =>{
   // this.setState({ submitted: true });
    const { searchCode } = this.state;
    alert('A name was submitted: '+searchCode); 
    if (searchCode) {
        const { dispatch } = this.props;
        dispatch(searchAction.getAuthors(searchCode));
    }
}

   render() {
    //const { classes } = this.props;
   // const { author } = this.props;
   
     return (
       <div>
         <MDBNavbar color="indigo" dark expand="md">
               <MDBNavbarBrand>
                 <strong className="white-text">Welcome </strong>
                </MDBNavbarBrand>
           </MDBNavbar>
         <MDBContainer>
         <MDBRow>
<MDBCol size="3"></MDBCol>
<MDBCol size="6">
<div className="text-center border border-light p-5 mt-3">
<p className="h4 mb-4">Search</p>
       <input
          type="text"
          id="defaultFormRegisterNameEx"
          className="form-control mb-4"
          name="searchCode"
          value={this.state.searchCode}
          onChange={this.handleChange}
          required/>
          <button className="btn btn-info btn-block" type="submit" onClick={(event)=>{this.getAuthors()}}>Search</button>
</div>
</MDBCol>
<MDBCol size="3"></MDBCol>
        </MDBRow>

        <MDBRow className="border-top">
            <MDBCol size="12">

            </MDBCol>
        </MDBRow>
         </MDBContainer>

       </div>
     );
   }
 }

const mapStateToProps = (state) =>{
  return {
    author : state.author
  };
}


connect(mapStateToProps, null, null, {
  pure: false
})(Home);

export default Home;