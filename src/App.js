import React, { Component } from 'react';
import './App.css';
import { Router, Switch, Route} from 'react-router-dom';
import { history } from './_helpers';
import  Home   from './_components/Home';


class App extends Component {
   render() {
      return (
          <div className="App">
              <Router history={history}>
                <div>
                    <Switch>
                       <Route exact path='/' component={Home} />
                    </Switch>
                </div>
              </Router>
          </div>
       );
    }
}
export default App;